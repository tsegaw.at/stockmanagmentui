import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import axios from 'axios'
import 'vuetify/dist/vuetify.min.css'
// import './../static/resets.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './components/common/common.css'
import 'babel-polyfill'
import storeData from './store'
import Vuelidate from 'vuelidate'

Vue.use(Vuex)
Vue.use(Vuelidate)
Vue.use(Vuetify, {
  theme: {
    primary: '#819ca9',
    darkPrimary: '#2a434d',
    secondary: '#3bbcd4',
    darkSecondary: '#2b8ba3',
    lightBackground: '#e1e7ea',
    lightBlueBackground: '#d3e1e8',
    accent: '#3bbcd4',
    error: '#FF6E40',
    info: '#212121',
    success: '#00897B',
    warning: '#f08a33',
    lightWarning: '#ff9100',
    grey: '#EEEEEE',
    lightSuccess: '#B2DFDB',
    lightError: '#FF6E40'
  }
})
Vue.config.productionTip = false
const token = localStorage.getItem('user-token')
if (token) {
  axios.defaults.headers.common['Authorization'] = token
}
const store = new Vuex.Store(storeData)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
export default store
