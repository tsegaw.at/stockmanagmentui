import Vue from 'vue'
import Router from 'vue-router'
import LoginComponent from './../components/login/login.vue'
import HomePageComponent from './../components/home/home.vue'
import ReportComponent from './../components/report/report.vue'
import SalesComponent from './../components/sales/sales.vue'
import StoreComponent from './../components/store/store.vue'
import AddItemsComponent from './../components/items/itemAdd.vue'
import AddSellerComponent from './../components/seller/sellerAdd.vue'
import AddUserComponent from './../components/user/userAdd.vue'
import ViewItemsComponent from './../components/items/itemView.vue'
import ViewSellerComponent from './../components/seller/sellerView.vue'
import ViewUserComponent from './../components/user/userView.vue'
import StockComponent from './../components/stock/stock.vue'
import ItemStock from './../components/stock/itemStock.vue'
Vue.use(Router)

const ifAuthenticated = (to, from, next) => {
  if (localStorage.getItem('user-token') !== null || localStorage.getItem('user-token') !== undefined) {
    next()
    return
  }
  next('/login')
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'LoginComponent',
      component: LoginComponent
    },
    {
      path: '/home',
      name: 'HomePageComponent',
      component: HomePageComponent,
      beforeEnter: ifAuthenticated,
      children: [
        {
          path: 'report',
          component: ReportComponent
        },
        {
          path: 'sales',
          component: SalesComponent
        },
        {
          path: 'store',
          component: StoreComponent
        },
        {
          path: 'item_view/:id',
          name: 'item_view',
          component: ViewItemsComponent
        },
        {
          path: 'item_add',
          component: AddItemsComponent
        },
        {
          path: 'seller_view/:id',
          name: 'seller_view',
          component: ViewSellerComponent
        },
        {
          path: 'seller_add',
          component: AddSellerComponent
        },
        {
          path: 'user_view/:id',
          name: 'user_view',
          component: ViewUserComponent
        },
        {
          path: 'user_add',
          component: AddUserComponent
        },
        {
          path: 'stock',
          component: StockComponent,
          children: [
            {
              path: 'stock_item/:id',
              name: 'stock_item',
              component: ItemStock
            }
          ]
        }
      ]
    }
  ]
})
