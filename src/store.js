import loginModule from './components/login/login.module'
import itemRegistrationModule from './components/items/itemRegistration.module'
import reportModule from './components/report/report.module'
import stockModule from './components/stock/stock.module'
import homeModule from './components/home/home.module'
export default {
  modules: {
    loginModule: loginModule,
    itemRegistrationModule: itemRegistrationModule,
    reportModule: reportModule,
    stockModule: stockModule,
    homeModule: homeModule
  },
  state: {
    errorModal: false,
    errorMessage: 'Oops ERROR ocurred',
    successModal: false,
    successMessage: 'Operation success',
    userType: localStorage.getItem('type') || ''
  },
  mutations: {
    SET_USER_TYPE: (state, type) => {
      localStorage.setItem('type', type)
      state.userType = type
    },
    CLEAR_USER_TYPE: (state, type) => {
      state.userType = localStorage.removeItem('type')
    },
    HIDE_ERROR_SNACK_BAR: (state) => {
      state.errorModal = false
    },
    SHOW_ERROR_SNACK_BAR: (state, passedMessage) => {
      state.errorModal = true
      state.errorMessage = passedMessage
    },
    HIDE_SUCCESS_SNACK_BAR: (state) => {
      state.successModal = false
    },
    SHOW_SUCCESS_SNACK_BAR: (state, passedMessage) => {
      state.successModal = true
      state.successMessage = passedMessage
      setTimeout(() => {
        state.successModal = false
      }, 6000)
    }
  }
}
