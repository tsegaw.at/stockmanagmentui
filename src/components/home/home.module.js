export default {
  state: {
    getAllItemList: true,
    getAllSellerList: true,
    getAllUserList: true
  },
  mutations: {
    UPDATE_ALL_ITEMS_LIST (state) {
      setTimeout(() => {
        state.getAllItemList = true
      }, 2000)
    },
    ITEMS_LIST_UPDATED (state) {
      setTimeout(() => {
        state.getAllItemList = false
      }, 2000)
    },
    UPDATE_ALL_SELLERS_LIST (state) {
      setTimeout(() => {
        state.getAllSellerList = true
      }, 2000)
    },
    SELLERS_LIST_UPDATED (state) {
      setTimeout(() => {
        state.getAllSellerList = false
      }, 2000)
    },
    UPDATE_ALL_USERS_LIST (state) {
      setTimeout(() => {
        state.getAllUserList = true
      }, 2000)
    },
    USERS_LIST_UPDATED (state) {
      setTimeout(() => {
        state.getAllUserList = false
      }, 2000)
    }
  }
}
