export default {
  state: {
    itemRegistrationFormValid: false,
    itemName: '',
    numberOfItems: 0,
    expectedSalesDate: undefined,
    storeFormValid: false,
    storeList: [],
    brandFormValid: false,
    sizePriceNumberFormValid: true,
    sizePriceNumberList: [],
    showRegisterItemButton: false,
    fullItemInformationObject: {},
    allStoresList: []
  },
  getters: {
  },
  mutations: {
    ALL_STORE_DATA_RETURNED (state, storesList) {
      state.allStoresList = storesList
    },
    RESET_ITEM_REGISTRATION_FORM (state) {
      state.itemRegistrationFormValid = false
      state.itemName = ''
      state.numberOfItems = 0
      state.expectedSalesDate = undefined
      state.storeFormValid = false
      state.storeList = []
      state.brandFormValid = false
      state.sizePriceNumberFormValid = true
      state.sizePriceNumberList = []
      state.allStoresList = []
      state.fullItemInformationObject = {}
    },
    ITEM_REGISTRATION_FORM_COMPLETE (state, registrationResult) {
      state.itemRegistrationFormValid = true
      state.itemName = registrationResult.itemName
      state.numberOfItems = registrationResult.numberOfItems
      state.expectedSalesDate = registrationResult.expectedSalesDate
      state.fullItemInformationObject.name = registrationResult.itemName
      state.fullItemInformationObject.expectedSalesDate = registrationResult.expectedSalesDate
      state.fullItemInformationObject.itemInfo = []
    },
    STORE_SELECTION_FORM_COMPLETE (state, storeList) {
      state.storeFormValid = true
      state.storeList = storeList.map(element => {
        return {
          storeName: element,
          brands: []
        }
      })
    },
    BRAND_REGISTRATION_FORM_COMPLETE (state, storeList) {
      state.brandFormValid = true
      state.storeList = storeList
    },
    SIZE_REGISTRATION_FORM_COMPLETE (state, sizePriceNumberList) {
      state.sizePriceNumberFormValid = false
      state.showRegisterItemButton = true
      state.sizePriceNumberList = sizePriceNumberList
      state.fullItemInformationObject.itemInfo = sizePriceNumberList
    },
    ON_VIEW_ITEM_RESPONSE (state, response) {
      state.showRegisterItemButton = false
      state.itemRegistrationFormValid = true
      state.fullItemInformationObject = response
    }
  },
  actions: {

  }
}
