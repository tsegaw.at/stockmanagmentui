import axios from 'axios'
export default {
  state: {
    token: localStorage.getItem('user-token') || '',
    status: ''
  },
  getters: {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status
  },
  mutations: {
    AUTH_REQUEST: (state) => {
      state.status = 'loading'
    },
    AUTH_SUCCESS: (state, token) => {
      state.status = 'success'
      state.token = token
    },
    AUTH_ERROR: (state) => {
      state.status = 'error'
    }
  },
  actions: {
    AUTH_REQUEST ({commit, dispatch}, user) {
      return new Promise((resolve, reject) => {
        commit('AUTH_REQUEST')
        axios({ url: '/login', data: user, method: 'POST' })
          .then(resp => {
            const token = resp.data.token
            localStorage.setItem('user-token', token)
            localStorage.setItem('user', resp.data.user.userName)
            commit('SET_USER_TYPE', resp.data.user.role)
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
            axios.defaults.headers.common['User'] = resp.data.user.userName
            commit('AUTH_SUCCESS', resp)
            resolve(resp)
          })
          .catch(err => {
            commit('AUTH_ERROR', err)
            localStorage.removeItem('user-token')
            localStorage.removeItem('user')
            commit('CLEAR_USER_TYPE')
            reject(err)
          })
      })
    },
    AUTH_LOGOUT: ({commit, dispatch}) => {
      return new Promise((resolve, reject) => {
        localStorage.removeItem('user-token')
        localStorage.removeItem('user')
        commit('CLEAR_USER_TYPE')
        delete axios.defaults.headers.common['Authorization']
        delete axios.defaults.headers.common['User']
        resolve()
      })
    }
  }
}
