import axios from 'axios'
export default {
  state: {
    salesReportData: [],
    tableSalesResult: [],
    noSalesDataFound: false,
    salesDataFound: false
  },
  mutations: {
    ITEM_SALES_DATA_REQUEST_FAILURE (state) {
      state.noSalesDataFound = false
      state.salesDataFound = true
    },
    ITEM_SALES_DATA_RETURNED (state, salesData) {
      if (salesData.length > 0) {
        state.noSalesDataFound = false
        state.salesDataFound = true
      } else {
        state.noSalesDataFound = true
        state.salesDataFound = false
      }
      state.salesReportData = salesData
      state.tableSalesResult = tableSalesResult()
      function tableSalesResult () {
        let salesReportData = salesData
        let tableSalesResult = []
        let brandsList = []
        salesReportData.forEach(element => {
          brandsList.push(element.name)
        })
        let filteredBrandList = brandsList.filter(function (item, pos) {
          return brandsList.indexOf(item) === pos
        })
        filteredBrandList.forEach(element => {
          let singleElement = {}
          salesReportData.forEach(salesElement => {
            if (salesElement.name === element) {
              if (singleElement.number === undefined) {
                singleElement = {
                  name: element,
                  salesPrice: salesElement.salesPrice,
                  totalSold: salesElement.salesPrice,
                  number: 1
                }
              } else {
                singleElement.number = singleElement.number + 1
                singleElement.totalSold = singleElement.totalSold + salesElement.salesPrice
              }
            }
          })
          tableSalesResult.push(singleElement)
        })
        return tableSalesResult
      }
    }
  },
  actions: {
    VIEW_ITEM_REQUEST ({commit, dispatch}, time) {
      return new Promise((resolve, reject) => {
        axios.get('sales/' + time.startTime + '/' + time.endTime)
          .then(response => {
            commit('ITEM_SALES_DATA_RETURNED', response.data)
            resolve(response)
          })
          .catch(error => {
            commit('ITEM_SALES_DATA_REQUEST_FAILURE')
            reject(error)
          })
      })
    }
  }
}
