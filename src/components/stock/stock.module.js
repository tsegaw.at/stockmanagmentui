export default {
  state: {
    moveItemFromToStore: false,
    moveItemInfo: {}
  },
  mutations: {
    ON_MOVE_ITEMS_CLICK (state, item) {
      state.moveItemFromToStore = true
      state.moveItemInfo = item
    },
    RESET_STOCK_MODULE_STATE (state) {
      state.moveItemFromToStore = false
      state.moveItemInfo = {}
    },
    CLOSE_MOVE_ITEMS_MODAL (state) {
      state.moveItemFromToStore = false
    }
  }
}
